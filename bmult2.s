.text
# Parametrii sunt dați prin stivă astfel:
#	0	   4		 8	    12		  16	    20
# sp -> | adr nr 1 | mărime nr 1 | adr nr 2 | mărime nr 2 | adr rez | mărime rez |
# ordinea octeților (și wordurilor) este de la cel mai nesemnificativ spre cel mai semnificativ.
# În $v0 pune 0 dacă nu au fost erori, -1 dacă mărimea unuia dintre numere este <= 0, sau mărimea necesară (în worduri) pentru a stoca rezultatul calculului în caz contrar.
# În $v1 pune, dacă nu au fost erori, mărimea numărului rezultat în worduri
bmult:	li $v0, -1
	lw $a0, 4($sp)		# mărime nr 1
	ble $a0, $0 err
	lw $a1, 12($sp)		# mărime nr2
	ble $a1, $0 err
	add $v0, $a0, $a1	# mărimea maximă necesară pt rezultat
	addi $a3, $v0, -1	# mărimea minimă necesară pt rezultat
	lw $a2, 20($sp)
	blt $a2, $a3 err	# dacă am mai puțin decât mărimea minimă nu e ok
	# inițializăm memoria pt locul unde stochez rezultatul (primele v0 worduri)
	lw $t2, 16($sp)
init0:	beq $v0, $0 endinit0
	sw $0, ($t2)
	addi $t2, 4
	addi $v0, -1
	j init0
endinit0:add $v0, $a0, $a1	# refac numărul din v0
	# Acum, înmulțim fiecare cifră a nr2 cu fiecare cifră a nr1
	# și adunăm produsele parțiale astfel obținute în rez. 
	# (ca la înmulțirea manuală)
	lw $t1, 8($sp)			# nr2
	sll $a1, $a1, 2
	add $a1, $t1, $a1	# ultima adresă din nr2
	sll $a0, $a0, 2
	lw $t0, 0($fp)
	add $a0, $a0, $t0	# ultima adresă din nr1
	lw $t2, 16($sp)		# iarăși adresa rezultatului; de aici începem adunarea produselor parțiale
loop1:	beq $t1, $a1 mulend:
	lw $a3, ($t1)			# cifra curentă din nr2
	xor $t8, $t8, $t8		# carryul la înmulțire
	xor $t9, $t9, $t9		# carryul la adunare
	move $t3, $t2
loop2:	beq $t0, $a0 prend
	beq $a2, $0 err		# verificăm dacă avem loc în rez
	lw $t4, ($t0)			# cifra curentă din nr1
	multu $a3, $t4			# o înmulțim cu cifra curentă din nr2
	mflo $t4
	addu $t4, $t4, $t8		# pun și carryul de la înmulțire
	sltu $t5, $t4, $t8		# dacă am avut overflow la adunare
	mfhi $t8
	addu $t8, $t8, $t5		# noul carry la înmulțire
	# acum adunăm ce este în t4 peste ce am calculat mai înainte în rez
	lw $t5, ($t3)	# cifra curentă din rez
	addu $t4, $t4, $t5
	sltu $t6, $t4, $t5		# văd dacă am overflow la adunarea cifrelor
	addu $t4, $t4, $t9	# adun și carryul de la adunare peste
	sltu $t9, $t4, $t9
	or $t9, $t9, $t6	# am calculat carryul următor pt adunare
	sw $t4, ($t3)		# pun înapoi calculul, peste ce era înainte
	addi $a2, -1
	addi $t0, 4
	addi $t3, 4
	j loop2
prend:	beq $t8, $0 ncr1	# salvăm ultimul carry la înmulțire
	beq $a2, $0 err
	addu $t8, $t8, $t9	# punem mai întâi carryul pt adunare peste
	sltu $t9, $t8, $t9	# și calculăm dacă avem un nou carry la adunare
	sw $t8, ($t3)
	addi $a2, -1
	addi $t3, 4
ncr1:	beq $t9, $0 ncr2	# salvăm și carryul la înmulțire
	beq $a2, $0 err
	sw $t9, ($t3)
	addi $t3, 4  # pt ca t3 să fie mereu prima poziție din rez în care nu am scris
ncr2:	addi $t1, 4
	addi $t2, 4	# avansăm cu o poziție mai la stânga în rezultat
	lw $a2, 20($sp)
	lw $t0, 16($sp)	# acum trebuie să vedem cât spațiu avem în rez de la
	sub $t0, $t2, $t0	# poziția t2 mai departe
	srl $t0, $t0, 2
	sub $a2, $a2, $t0
	lw $t0, 0($sp)
	j loop1
mulend: lw $v1, 16($sp)
	sub $v1, $t3, $v1
	srl $v1, $v1, 2		# mărimea rezultatului
	xor $v0, $v0, $v0		# semnalăm că am ieșit cu succes
err:	jr $ra

main:	addi $sp, -24
	la $t0, rez
	sw $t0, 16($sp)
	lw $t1, srez
	sw $t1, 20($sp)
	la $t0, nr2
	sw $t0, 8($sp)
	lw $t1, snr2
	sw $t1, 12($sp)
	la $t0, nr1
	sw $t0, 0($sp)
	lw $t1, snr1
	sw $t1, 4($sp)
	jal bmult
	move $s0, $v0
	addi $sp, 24
	li $v0, 10
	syscall

.data
	.word 0xdeaddead
rez:	.word 0, 0, 0, 0, 0, 0, 0
	.word 0xdeaddead
nr1:	.word 1, 1, 1, 1, 0x80000000
	.word 0xdeadbeef
nr2:	.word 2, 3
	.word 0xdeadbeef
snr1:	.word 5
snr2:	.word 2
srez:	.word 7
	.word 0xbeefbeef
