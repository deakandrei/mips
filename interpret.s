.text
# a0 = începutul șirului asciiz
# a1 = tipul de caracter pe care îl sărim
# Avansează a0 până la primul caracter care nu este a1
trim_character:
	lb $t9, ($a0)
	bne $t9, $a1 trim_ok
	addi $a0, 1
	j trim_character
trim_ok: jr $ra

# a0 = șirul
# întoarce prin v0 numărul citit
# sau sare direct la errcom în caz de eroare
read_number:
	xor $v0, $v0, $v0
	ori $v1, $0, 10
	lb $t0, ($a0)
	xor $t1, $t1, $t1
	beq $t0, $a1 errcom
	beq $t0, $0 errcom
	beq $t0, 10 errcom
	bne $t0, '-' pozitive
	ori $t1, $0, 1
	addi $a0, 1
	lb $t0, ($a0)
pozitive:beq $t0, $a1 readok		# dacă am ajuns la spațiu
	beq $t0, $0 readok		# sau la sfârșitul șirului
	beq $t0, 10 readok		# tratăm newline-ul ca sfârșit de șir
	sub $t0, $t0, '0'
	blt $t0, $0 errcom
	bgt $t0, 9 errcom		# dacă nu suntem între limitele astea, atunci caracterul nu era o cifră
	multu $v0, $v1
	mflo $v0
	add $v0, $v0, $t0
	blt $v0, $0 errcom		# în cazul ăsta, v0 este un întreg pozitiv care nu încape ca signed word
	mfhi $t0
	bne $t0, $0 errcom		# am avut overflow la înmulțire
	addi $a0, 1
	lb $t0, ($a0)
	sll $t1, $t1, 1
	j pozitive
readok:	beq $t1, $0 readend
	beq $t1, 1 errcom		# în cazul ăsta, numărul era de fapt doar un '-'
	sub $v0, $0, $v0
readend:jr $ra

addcom:	xor $t2, $t2, $t2
addl:	jal trim_character
	lb $t0, ($a0)
	beq $t0, $0 addok		# dacă suntem la sfârșitul liniei
	beq $t0, 10 addok
	jal read_number
	add $t2, $t2, $v0
	j addl
addok:	move $a0, $t2
	li $v0, 1
	syscall
	li $v0, 4
	la $a0, newline
	syscall
	jr $t8

check_end:
	lb $t0, ($a0)
	beq $t0, $0 yes
	beq $t0, 10 yes
	j errcom			# dacă nu avem newline sau \0 atunci clar șirul continuă
yes:	jr $ra

subcom:	lb $t0, ($a0)
	bne $t0, $a1 errcom
	jal trim_character
	jal read_number
	move $t2, $v0
	jal trim_character
	jal read_number
	jal trim_character
	jal check_end
	sub $a0, $t2, $v0
	li $v0, 1
	syscall
	li $v0, 4
	la $a0, newline
	syscall
	jr $t8

negcom:	lb $t0, ($a0)
	bne $t0, $a1 errcom
	jal trim_character
	jal read_number
	jal trim_character
	jal check_end
	sub $a0, $0, $v0
	li $v0, 1
	syscall
	li $v0, 4
	la $a0, newline
	syscall
	jr $t8

endcom:	la $a0, finish
	li $v0, 4
	syscall
	li $v0, 10
	syscall

errcom:	la $a0, errmsg
	li $v0, 4
	syscall
	jr $t8

.data
jump_table:	.word	addcom, subcom, negcom, endcom, errcom
.text
main:	la $t8, bigloop
bigloop:li $v0, 8
	la $a0, str
	li $a1, 256
	syscall
	li $a1, ' '
	jal trim_character
	lb $t0, ($a0)
	addi $a0, 1			# get this character out of the string
	li $t1, -1
loop:	addi $t1, 1
	beq $t1, 4 ok
	lb $t2, commands($t1)
	bne $t0, $t2 loop
ok:	sll $t1, $t1, 2
	lw $t0, jump_table($t1)
	jr $t0

.data
# EOF = 0x04 = \04
str:	.space	256
newline: .asciiz "\n"
finish:	.asciiz	"La revedere\n"
errmsg:	.asciiz	"Eroare\n"
commands: .byte	'a', 's', 'n', 0x04, 0x00	# eroare dacă ajungem la ultimul
