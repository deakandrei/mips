.data
R:	.space	36	# produsul m * p * 4

A:	.word	1, 2
	.word	3, 4

B:	.word	2, 0
	.word	0, 2


m:	.word	2	# liniile matricii A
n:	.word	2	# coloanele matricii A și liniiile maricii B
p:	.word	2	# coloanele matricii B
.text
.globl main
main:
	li $s3, 2	# o să-l folosesc la shifturi
	lw $s0, m	# matricea rezultat va avea m linii
	lw $s1, p	# și p coloane
	lw $s2, n
	li $t0, -1
for1:	addi $t0, 1
	beq $t0, $s0, END	# while(t0 < s0), presupunând că la început avem t0 < s0
	and $t1, $t1, $0
for2:	beq $t1, $s1, for1	# while(t1 < s1)
	and $t3, $t3, 0	# aici o să calculez R[t0][t1]
	xor $t2, $t2, $t2
for3:	beq $t2, $s2, store	# while(t2 < s2)
	# tre să calculesz în t4 offsetul pentru A[t0][t2] = A + 4 * (t0 * n + t2)
	mult $t0, $s2 # fac t0 * n // ăsta ar putea fi salvat printr-un alt registru...
	mflo $t4
	add $t4, $t4, $t2  # acum t4 = t0 * n + t2
	sll $t4, $t4, $s3 # înmulțesc cu 4
	lw $t4, A($t4) # acum nu mai am nevoie de adresa asta, deci pot să o suprascriu
	# calculăm în t5 offsetul pentru B[t2][t1] = B + 4 * (t2 * p + t1)
	mult $t2, $s1
	mflo $t5
	add $t5, $t5, $t1
	sll $t5, $t5, $s3 # înmulțesc cu 4
	lw $t5, B($t5) # și în sfârșit am citit cele 2 numere din matrice
	mult $t4, $t5
	mflo $t4 # să sperăm că nu dă un număr peste 32 de biți...
	add $t3, $t3, $t4
	addi $t2, 1
	j for3
store:	# calculez R[t0][t1] = R + 4 * (t0 * p + t1) în t4
	mult $t0, $s1
	mflo $t4
	add $t4, $t4, $t1
	sll $t4, $t4, $s3 # gata
	sw $t3, R($t4)
	addi $t1, 1
	j for2
END:
	li $v0, 10
	syscall
