.text
.globl main
# Primește prin v0 vectorul sursă, care este împărțit în 2 vectori ca mai jos,
# prin v1 vectorul destinație, prin a0 adresa unde se termină primul vector și prin
# a1 adresa unde se termină al doilea vector.
# Vectorii arată așa:
#                          a0                   a1
# v0 -> |  primul vector   |   al doilea vector |
# v1 -> |             spațiu liber              |
# După executare, în v1 se vor găsi primul vector și al doilea vector interclasați.
# De asemenea, valorile din v0 și v1 _NU_ sunt păstrate de funcție!!!
merge:	# modifică pe v0, v1, t0, t1, t2
	#xor $t0, $v0	# v0, t0 și v1 parcurg vectorii sursă1, sursă2 respectiv destinație
	move $t0, $a0	# vectorul destinație (v1)
mwhile:	bge $v0, $a0, endmerge # while(v0 < a0 && t0 < a1)
	bge $t0, $a1, endmerge
	lw $t1, ($v0)	# citește primul număr din vectorul 1
	lw $t2, ($t0)	# citește primul număr din vectorul 2
	blt $t1, $t2, ifcase # vrem să vedem care este mai mic
	# ramura else... în cazul ăsta, t2 < t1
	sw $t2, ($v1)
	addi $t0, 4	# înaintăm in vectorul 2
	addi $v1, 4	# și în vectorul destinație
	j mwhile	# o luăm de la capăt cu while-ul
ifcase:	# ramura if
	sw $t1, ($v1)
	addi $v0, 4
	addi $v1, 4
	j mwhile
endmerge: # cel puțin unul dintre cei 2 vectori sursă s-a terminat
	bge $v0, $a0, endv1	# while(v0 < a0)
	lw $t1, ($v0)
	sw $t1, ($v1)
	addi $v0, 4
	addi $v1, 4
	j endmerge # acolo începe while-ul nostru
endv1:
	bge $t0, $a1, reallythend #while(t0 < a1)
	lw $t1, ($t0)
	sw $t1, ($v1)
	addi $t0, 4
	addi $v1, 4
	j endv1
reallythend:
	jr $ra # ra este valid pentru că nu am apelat pe nimeni din funcția asta

# Primește un vector oarecare prin a0, sfărșitul său prin a1 și îl sortează inplace
# folosind un vector auxiliar de aceeași mărime dat prin a2.
# a1 arată de fapt către prima adresă care nu este din vector!
# Nu este garantat că a0 sau a1 vor avea aceeași valoare la final!
sort:
	# Să vedem câte elemente sînt în vector...
	sub $t0, $a1, $a0 # vedem care este diferența dintre început și sfârșit
	li $t1, 2
	srl $t0, $t0, $t1 # și o împărțim la 4
	ble $t0, $t1, easycase # Dacă avem mai puțin de 2 elemente în vector
	# Altfel împărțim vectorul în două
	li $t3, 1
	srl $t0, $t0, $t3 # împart la 2 cu rotunjire în jos
	sll $t0, $t0, $t1 # Înmulțesc cu 4
	addi $sp, -20 # facem loc pentru toate variabilele pe care vrem să le păstrăm
	sw $ra, 16($sp)
	sw $t0, 12($sp)
	sw $a2, 8($sp)
	sw $a1, 4($sp)
	sw $a0, 0($sp)
	# Acum că am salvat ce este important, apelăm sort recursiv
	add $a1, $a0, $t0 # calculăm adresa de mijloc din ambii vectori
	jal sort # partea stângă
	lw $a0, 0($sp) # sp trebuie să fie la fel ca atunci când am apelat sort!!
	lw $a1, 4($sp)
	lw $a2, 8($sp)
	lw $t0, 12($sp)
	add $a0, $a0, $t0
	add $a2, $a2, $t0
	jal sort # partea dreaptă
	# Acum apelăm merge
	lw $v0, 0($sp) # valoarea lui a0, vectorul din stânga (v0 <--> a0)
	lw $a1, 4($sp) # chiar valoarea bună pentru a apela merge (a1 <--> a1)
	lw $v1, 8($sp) # începutul vectorului auxiliar (v2 <--> a2)
	lw $t0, 12($sp) # folosit să recalculăm mijlocul din vectorul sursă
	add $a0, $v0, $t0 # locul în care sunt separați cei 2 vectori sortați (a0 <--> a0 + t0)
	jal merge
	# Copiem din auxiliar înapoi în vectorul sursă
	lw $a0, 0($sp)
	lw $a1, 4($sp)
	lw $a2, 8($sp)
	lw $ra, 16($sp) # jr $ra face ce trebuie acum
	addi $sp, 20 # am șters variabilele din stivă
wback:	bge $a0, $a1, bye # while (a2 < a3)
	lw $t0, ($a2)
	sw $t0, ($a0)
	addi $a0, 4
	addi $a2, 4
	j wback
easycase:
	li $t2, 4
	sub $t0, $a1, $t2	# ultima poziție din sursă
	lw $t4, ($a0) # citesc valorile din vectorul sursă
	lw $t5, ($t0)
	blt $t4, $t5, c1 	# dacă t4 < t5
	sw $t5, ($a0) # le scriu sortate în sursă
	sw $t4, ($t0)
	jr $ra
c1:	sw $t4, ($a0)
	sw $t5, ($t0)
bye:	jr $ra

main:	# regiștrii importanți: sp, s7
	li $v0, 4	# apeleaza print_string
	la $a0, msg1
	syscall
	addi $v0, 1 # citim nr de elemente -- acum v0 este 5
	syscall
	move $s7, $v0	# salvăm nr de elemente (s7 == n)
	li $v0, 4	# iarăși printăm un string
	la $a0, msg2
	syscall
	move $a1, $sp	# salvez sfârșitul vectorului
	xor $t0, $t0, $t0	# contor care numără câte elemente am citit
readw:	bge $t0, $s7, readf # while(t0 < s7)
	li $v0, 5
	syscall
	addi $sp, -4	# salvăm numerele în stivă pe măsură ce le citim!
	sw $v0, ($sp)
	addi $t0, 1
	j readw
readf:	move $a0, $sp	# salvez începutul vectorului citit
	beq $t0, $0, thegloriousend	# înseamnă că s7 este <= 0, deci nu am ce să sortez
	li $t1, 2
	sll $t0, $t0, $t1 # înmulțesc cu 4 numărul de elemente al vectorului ca să-l scad din
	sub $sp, $sp, $t0 # sp și să obțin o adresă aliniată la un word.
	# Si așa am mai făcut un vector în stivă
	move $a2, $sp # acesta este vectorul auxiliar de care are nevoie sort
	jal sort # și acum după toate pregătirile, putem apela sort
	# Acum stiva arată așa:
	# sp --> | vector auxiliar | vector sortat |
	# unde numărul de elemente din fiecare vector este egal cu valoarea din $s7
	li $t0, 2
	sll $s7, $s7, $t0
	add $sp, $sp, $s7 # scap de primul vector
	add $s7, $s7, $sp # transform s7 în adresa la care era sp înainte de apelul la main
	# Începen sa afișăm rezultatele
	li $v0, 4
	la $a0, msg3
	syscall
printw:	bge $sp, $s7, thegloriousend
	li $v0, 1
	lw $a0, ($sp) # scot elementele din vector unul câte unul
	syscall		# și le afișez
	addi $sp, 4
	li $v0, 4	# printez un spațiu să se înțeleagă numerele
	la $a0, space
	syscall
	j printw
thegloriousend:	li $v0, 10 # sfărșit
	syscall

.data
msg1:	.asciiz	"Introduceti numarul de elemente n = "
msg2:	.asciiz	"Introduceti elementele cate unul pe rand:\n"
msg3:	.asciiz	"Vectorul sortat este:\n"
space:	.asciiz " "
