.data
n:	.word	0xb290ad4b
.text

main:
	lw $t0, n
	move $t7, $t0
	li $t1, 16
	nor $t2 $0, $0			# fac toți biții 1
	srl $t2, $t2, $t1		# acum $t2 este pe jumătate 0 și pe jumătate 1
loop:	beq $t1, $0, final
	and $t3, $t0, $t2		# pun biții de jos în t3
	not $t2, $t2
	and $t4, $t0, $t2		# și biții de sus în t4
	sll $t3, $t3, $t1
	srl $t4, $t4, $t1
	or $t0, $t3, $t4		# și i-am pus la loc pe dos
	li $t3, 1
	srl $t1, $t1, $t3
	not $t3, $t2
	sll $t3, $t3, $t1
	and $t2, $t2, $t3
	srl $t3, $t2, $t1
	srl $t3, $t3, $t1
	or $t2, $t2, $t3
	j loop
final:	sw $t0, n
	li $v0, 10
	syscall
