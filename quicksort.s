.text
# Argumentele sunt date prin stivă în modul următor:
# sp -> | adresa de început | adresa de final |
# adică 0($sp) e adresa de început și 4($sp) e adresa de final
quicksort: # alegem mereu pivotul să fie primul element
	lw $t0, 0($sp)
	lw $t2, 4($sp)	# finalul vectorului
	# să vedem dacă nu cumva vectorul nostru are unul sau două elemente
	sub $t1, $t2, $t0 # chestia asta mai trebuie împărțită la 4 ca să am aici nr de elemente
	li $t3, 2
	srl $t1, $t1, $t3 # și așa fac împărțirea
	ble $t1, $t3, trivial	# dacă am mai puțin de 2 elemente
	# dacă nu-i trivial, ghinion...
	lw $t7, ($t0)	# ăsta este pivotul
	addi $t0, $t0, 4# aici va fi adresa unde trebuie să despart vectorul în două (primul element mai mare ca pivotul)
	move $t1, $t0	# ăsta va fi indicele (îl refolosim pe t1)
	# la fiecare iterație vectorul este împărțit astfel:
	# [ pivot = $t7 ][ numere < pivotul ][numere > pivotul ][ neexplorat ]      --- partea neexplorată se micșorează cu 1 de fiecare dată până când dispare
	# unde t1 este la începutul părții neexplorate, t0 la începutul numerelor mai >.
divide:	beq $t1, $t2, conquer # dacă am ajuns la capăt ne oprim
	lw $t6, ($t1)	# citesc un număr
	blt $t6, $t7, cd
	# dacă numărul e mai mare ca pivotul îl las în pace
	addi $t1, $t1, 4 # trecem la următorul
	j divide
cd:	# dacă numărul e mai mic ca pivotul, atunci intershimbăm valoarea de la t0 cu valoarea de la t1
	lw $t5, ($t0)	# citim ce este în t0
	sw $t6, ($t0)
	sw $t5, ($t1)	# și le punem la loc pe dos
	addi $t1, $t1, 4
	addi $t0, $t0, 4 # mai avem un număr mai mic ca pivotul
	j divide	# și o luăm de la capăt
conquer:	# acum vectorul arată [ pivot][ numere < pivotul ][ numere > pivotul ]
	# și t0 este la granița dintre numerele < și numerele >, mai precis este pe primul număr mai mare
	addi $t0, $t0, -4 # îl pun pe t0 pe ultimul număr < ca pivotul pt că vreau să pun pivotul la locul lui în vector
	lw $t1, 0($sp)  # adresa pivotului
	lw $t6, ($t0)
	sw $t7, ($t0)	# pun elementele invers în vector
	sw $t6, ($t1)
	# acum trebuie doar să apelez de 2 ori quicksort
	# deci salvăm ra și cu ocazia asta punem și argumentele quicksortului în stivă
	addi $sp, -16	# loc pentru ra și pentru cele 2 argumente și pentru valoarea lui t0 pe care ar fi bine sa nu o pierd
	sw $ra, 12($sp)
	sw $t0, 8($sp)
	# și acum pentru primul apel...
	sw $t1, 0($sp)	# începutul vectorului
	sw $t0, 4($sp)	# sfârșitul primei jumătăți
	jal quicksort
	# acum quicksort a folosit regiștrii mei, dar de aia i-am salvat
	lw $t0, 8($sp)	# chiar adresa pivotului
	addi $t0, $t0, 4 # pe care îl lăsăm în pace
	sw $t0, 0($sp)	# acum ăsta e noul început
	lw $t1, 20($sp)	# valoarea care ne-a fost dată ca al 2-lea parametru, adică sfârșitul vectorului
		# !!!este la 20($sp) pentru că am mai pus înca 16 bytes în stivă!!!
	sw $t1, 4($sp)	# și pe care o pasăm la rândul nostru mai departe
	jal quicksort
	lw $ra, 12($sp)	# vrem să ne întoarcem
	addi $sp, 16	# lăsăm stiva cum am găsit-o (inclusiv cu parametrii care ne-au fost pasați, care întâmplător sunt nealterați)
	jr $ra	# și asta a fost
trivial:# aici nu trebuie decât să compar 2 numere
	# în t3 este valoarea 2 (vezi începutul funcției, de unde sare la trivial)
	beq $t1, $t3, twoel # dacă am mai puțin de 2 elemente, nu am nimic de făcut
	jr $ra	# este OK pentru că nu am apelat pe nimeni
twoel:	lw $t1, ($t0)
	addi $t2, $t2, -4 # luăm ultimul element din vectorul nostru care știm că are exact 2 elemente
	lw $t3, ($t2)
	bge $t1, $t3, cmps # dacă t1 > t2 sărim
	jr $ra	# pentru că altfel, iar nu am nimic de făcut că elementele sunt în ordinea bună
cmps:	sw $t1, ($t2)
	sw $t3, ($t0)
	jr $ra

main:	la $s0, vec
	lw $s1, size
	add $s1, $s1, $s1
	add $s1, $s1, $s1 # am înmulțit cu 4 mărimea vectorului
	add $s1, $s0, $s1 # acum avem în t1 prima adresă de după vector
	addi $sp, -8	# loc pentru 2 argumente word
	sw $s0, ($sp)
	sw $s1, 4($sp)	# și acum putem apela quicksort
	jal quicksort
	# și o să afișez vectorul în consolă
	addi $sp, 8	# las stiva cum era la început
	# quicksort nu folosește la nimic regiștrii cu s, deci valoarea lui s0, s1 este neschimbată
printl:	beq $s0, $s1, end
	lw $a0, ($s0)
	ori $v0, $0, 1	# pun 1 în v0, syscall-ul cu print
	syscall
	ori $v0, $0, 4  # print_string syscall
	la $a0, space	# pun valoarea din sp în a0, adică începutul stringului meu
	syscall
	addi $s0, 4
	j printl
end:	li $v0, 10
	syscall

.data
vec:	.word	3, 2, 5, 1, 7, 4, 9, 6, 8
size:	.word	9
space:	.asciiz	" "
