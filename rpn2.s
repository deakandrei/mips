# Transformă o expresie în forma poloneză inversă
# De exemplu: (123 - 43) * 343 - 33 / 343 - (13 - 4 * 4) * 5
# Se transformă în: 123 43 - 343 * 33 343 / - 13 4 4 * - 5 * -
.text
# v1 = începutul șirului asciiz
# t8 = tipul de caracter pe care îl sărim
# Avansează v1 până la primul caracter care nu este egal cu t8.
trim_spaces:
	lb $t9, ($v1)
	li $t8, ' '
	bne $t9, $t8 trim_ok
	addi $v1, 1
	j trim_spaces
trim_ok: jr $ra


# Stiva: sp -> | adresa vectorului sursă | adresa vectorului destinație | mărimea vectorului destinație |
# În caz de eroare, v0 va indica spre primul caracter la care s-a oprit citirea șirului sursă
# și v1 va fi egal cu numărul de caractere nescrise din v0.
# Altfel, v0 va fi 0.
transform:
	lw $v1, ($sp)
	lw $v0, 4($sp)
	lw $a0, 8($sp)
	sw $fp, -4($sp)
	sw $ra, -8($sp)
	addi $fp, $sp, -8
	beq $v0, $0 error	# niciuna din adrese nu trebuie să fie nulă
	beq $v1, $0 error
	ble $a0, $0 error
	jal expr
	lb $t0, ($v1)	# dacă nu am ajuns la sfârșitul vectorului din care citim, înseamnă că parantezele nu sunt puse corect
	bne $t0, $0 error
	beq $a0, $0 error
	addi $a0, -1
	sb $0, ($v0)
	xor $v0, $v0, $v0 	# semnalăm că am ieșit cu success
error:	move $v1, $a0
	lw $ra, -8($sp)
	lw $fp, -4($sp)
	jr $ra

# v0 - șirul pe care-l construim
# v1 - șirul sursă
expr:	addi $fp, -8		# fp -> | operația curentă | $ra |
	sw $ra, 4($fp)
	sw $0, ($fp)
eloop:	jal term		# citim un termen mai întâi
	lw $t0, ($fp)
	beq $t0, $0 emoveon	# scriu operația dintre ultimii doi termeni (dacă am citit cel puțin 2 termeni)
	addi $a0, -2
	blt $a0, $0 error	# mă asigur că am suficient spațiu înainte să scriu
	li $t1, ' '
	sb $t1, ($v0)
	sb $t0, 1($v0)
	addi $v0, 2
emoveon:jal trim_spaces
	# acum ce citim (dacă mai este ceva, trebuie să fie o operație aditivă)
	lb $t0, ($v1)
	beq $t0, '+' opv
	beq $t0, '-' opv
	j final
opv:	addi $v1, 1
	sw $t0, ($fp)	# reținem operația
	j eloop		# și mai citim încă un termen
final:	addi $fp, 8
	lw $ra, -4($fp)
	jr $ra

# Citim un termen
term:	addi $fp, -8
	sw $ra, 4($fp)
	sw $0, ($fp)
tloop:	jal fact
	lw $t0, ($fp)
	beq $t0, $0 tmoveon
	addi $a0, -2
	blt $a0, $0 error	# dacă nu avem loc în vectorul destinație, ghinion...
	li $t1, ' '
	sb $t1, ($v0)
	sb $t0, 1($v0)
	addi $v0, 2
tmoveon:jal trim_spaces
	lb $t0, ($v1)
	beq $t0, '*' mvl
	beq $t0, '/' mvl
	j fterm
mvl:	addi $v1, 1
	sw $t0, ($fp)
	j tloop
fterm:	addi $fp, 8
	lw $ra, -4($fp)
	jr $ra

# Citim un factor
fact:	addi $fp, -4
	sw $ra, ($fp)
	jal trim_spaces
	lb $t0, ($v1)
	bne $t0, '(' notexpr	# dacă primul caracter din factor este '(', atunci avem de a face de fapt cu o expresie
	addi $v1, 1
	jal expr
	jal trim_spaces
	lb $t0, ($v1)
	bne $t0, ')' error	# chiar dacă sărim de aici, din cauză că folosim fp, sp este practic neschimbat, deci știm de unde să luăm adresa apelantului inițial
	addi $v1, 1
	j ok
notexpr:beq $a0, $0 error
	addi $a0, -1
	li $t1, ' '
	sb $t1, ($v0)
	addi $v0, 1
	xor $t1, $t1, $t1
rstrt:	blt $t0, '0' rend
	bgt $t0, '9' rend
	beq $a0, $0 error
	addi $a0, -1
	sb $t0, ($v0)
	nor $t1, $0, $0
	addi $v0, 1
	addi $v1, 1
	lb $t0, ($v1)
	j rstrt
rend:	bne $t1, $0 ok
	addi $a0, 1
	addi $v0, -1
ok:	addi $fp, 4
	lw $ra, -4($fp)
	jr $ra


main:	la $t0, src
	la $t1, dest
	addi $sp, -12
	sw $t0, ($sp)
	sw $t1, 4($sp)
	li $t0, 256
	sw $t0, 8($sp)
	jal transform
	lw $a0, 4($sp)
	beq $v0, $0 print
	la $a0, errmsg
	bgt $v1, $0 print
	la $a0, errmsg2
print:	li $v0, 4
	syscall
	addi $sp, 12
	li $v0, 10
	syscall

.data
dest:	.space	256
src:	.asciiz "232 - 233209 ) * 23"
	.asciiz	"(123 - 43) * 343 * (43 - 2) - 33 / 343 - (13 - 4 * 4) * 5"
	.asciiz	"(1 + 1 / 5 * 4) + 343"
errmsg:	.asciiz	"Expresia este invalida"
errmsg2:.asciiz "Memorie insuficienta"
